import pandas as pd
import importlib
from sklearn.model_selection import train_test_split
import numpy as np

"""
Three different cases are implemented at the moment:

IMPORTER: The standard simple dataset from the /datasets/ folder

GENERATOR: Generating our own data using DOE, which allows the user to change the design, as well as the rates if this is of interest

IMPORT CHALLENGE: Generating a more challenging dataset, which the user can choose the number of runs, the train-test-split ratio and some artificial
noise to make the data more interesting to model. The noise is currently proportional to the value found in the owu matrix.

"""


def importer(filename, filepath):
    # Import OWU data
    doe = pd.read_csv(
        filepath + filename.replace(".csv", "_doe.csv"),
        index_col=None,
        usecols=["feed_start", "feed_end", "Glc_feed_rate", "Glc_0", "VCD_0"],
    )
    owu = pd.read_csv(
        filepath + filename,
        index_col=None,
        usecols=["X:VCD", "X:Glc", "X:Lac", "X:Titer", "W:Feed"],
    )
    owu.index = pd.MultiIndex.from_product(
        [list(range(int(len(owu) / 15))), list(range(15))], names=["run", "time"]
    )
    # Import OWU Test data
    filename = "owu_test.csv"
    doe_test = pd.read_csv(
        filepath + filename.replace(".csv", "_doe.csv"),
        index_col=None,
        usecols=["feed_start", "feed_end", "Glc_feed_rate", "Glc_0", "VCD_0"],
    )
    owu_test = pd.read_csv(
        filepath + filename,
        index_col=None,
        usecols=["X:VCD", "X:Glc", "X:Lac", "X:Titer", "W:Feed"],
    )
    owu_test.index = pd.MultiIndex.from_product(
        [list(range(int(len(owu) / 15))), list(range(15))], names=["run", "time"]
    )
    return doe, owu, doe_test, owu_test


def generator(var_lims, num_runs):
    simulator = importlib.import_module("chemometrics_2023.modules.simulator")
    # Filename and filepath for the dataset
    FILENAME = "owu.csv"
    FILEPATH = ""

    # Generate Dataset
    data = simulator.generate_data(var_lims, num_runs, FILENAME)
    # Import DOE
    doe = pd.read_csv(
        FILEPATH + FILENAME.replace(".csv", "_doe.csv"),
        index_col=None,
        usecols=["feed_start", "feed_end", "Glc_feed_rate", "Glc_0", "VCD_0"],
    )
    # Import OWU
    owu = pd.read_csv(
        FILEPATH + FILENAME,
        index_col=None,
        usecols=["X:VCD", "X:Glc", "X:Lac", "X:Titer", "W:Feed"],
    )
    owu.index = pd.MultiIndex.from_product(
        [list(range(num_runs)), list(range(15))], names=["run", "time"]
    )

    # Import OWU Test data
    filename = "owu.csv"
    filepath = "chemometrics_2023/datasets/"
    doe_test = pd.read_csv(
        filepath + filename.replace(".csv", "_doe.csv"),
        index_col=None,
        usecols=["feed_start", "feed_end", "Glc_feed_rate", "Glc_0", "VCD_0"],
    )
    owu_test = pd.read_csv(
        filepath + filename,
        index_col=None,
        usecols=["X:VCD", "X:Glc", "X:Lac", "X:Titer", "W:Feed"],
    )
    owu_test.index = pd.MultiIndex.from_product(
        [list(range(int(len(owu_test) / 15))), list(range(15))], names=["run", "time"]
    )
    return doe, owu, doe_test, owu_test


def import_challenge(num_runs, train_test_ratio, noise_level):
    MU_G_MAX = [0.04, 0.05]
    MU_D_MAX = [0.02, 0.03]
    K_G_GLC = [0.8, 1.2]
    K_I_LAC = [25, 35]
    K_D_LAC = [45, 55]
    K_GLC = [0.03, 0.04]
    K_LAC = [0.05, 0.07]
    K_PROD = [0.9, 1]
    # Process parameters: Conditions at which process is run
    FEED_START = [1, 4]
    FEED_END = [8, 12]
    GLC_FEED_RATE = [5, 20]
    GLC_0 = [10, 80.0]
    VCD_0 = [0.1, 1.0]
    # Collect parameters to dictionary
    VAR_LIMS = {
        "mu_g_max": MU_G_MAX,
        "mu_d_max": MU_D_MAX,
        "K_g_Glc": K_G_GLC,
        "K_I_Lac": K_I_LAC,
        "K_d_Lac": K_D_LAC,
        "k_Glc": K_GLC,
        "k_Lac": K_LAC,
        "k_Prod": K_PROD,
        "feed_start": FEED_START,
        "feed_end": FEED_END,
        "Glc_feed_rate": GLC_FEED_RATE,
        "Glc_0": GLC_0,
        "VCD_0": VCD_0,
    }

    # Generating our data
    doe, owu, _, _ = generator(VAR_LIMS, num_runs)

    # Adding noise to our data
    np.random.seed(0)
    owu = owu + abs(noise_level * owu * np.random.randn(owu.shape[0], owu.shape[1]))

    # Split our data into train and test
    train_ix, test_ix = train_test_split(
        owu.index.levels[0],
        train_size=train_test_ratio,
        random_state=0,
    )
    owu_train = owu.loc[train_ix]
    owu_test = owu.loc[test_ix]

    # Overwrite our owu matrix index with the correct multiindex
    owu_test.index = pd.MultiIndex.from_product(
        [list(range(int(len(owu_test) / 15))), list(range(15))], names=["run", "time"]
    )

    owu_train.index = pd.MultiIndex.from_product(
        [list(range(int(len(owu_train) / 15))), list(range(15))], names=["run", "time"]
    )

    doe_train = doe.loc[train_ix]
    doe_test = doe.loc[test_ix]
    doe_train.index = list(range(int(len(owu_train) / 15)))
    doe_test.index = list(range(int(len(owu_test) / 15)))
    return doe_train, owu_train, doe_test, owu_test
